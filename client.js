const EventEmitter     = require('events').EventEmitter;
const util             = require('util');
const randomstring     = require('randomstring');
const RequestMessage   = require('./messages').Request;
const ResponseMessage   = require('./messages').Response;
const ResponseDecoder  = require('./messages/decoders').ResponseDecoder;
const url              = require('url');

const disyuntor        = require('disyuntor');
const axios            = require('axios')

const ms = require('ms');
const _  = require('lodash');

const DEFAULT_PROTOCOL = 'http';
const DEFAULT_PORT  = 9485;
const DEFAULT_HOST  = 'localhost';

const lib_map = {
  'http': axios
};

function parseURI (uri) {
  const parsed = url.parse(uri);
  return {
    host: parsed.hostname,
    port: parseInt(parsed.port || DEFAULT_PORT, 10),
    protocol: parsed.protocol.slice(0, -1)
  };
}

function BaaSClient (options, done) {
  options = options || {};
  EventEmitter.call(this);

  if (typeof options === 'string') {
    options = parseURI(options);
  } else if (options.uri || options.url) {
    options = _.extend(options, parseURI(options.uri || options.url));
  } else {
    options.protocol = options.protocol || DEFAULT_PROTOCOL;
    options.port = options.port || DEFAULT_PORT;
    options.host = options.host || DEFAULT_HOST;
  }

  if (typeof options.url === 'undefined') {
    options.url = url.format({
      hostname: options.host,
      port: options.port,
      protocol: options.protocol
    });
  }

  this._socketLib = lib_map[options.protocol];

  if (!this._socketLib) {
    throw new Error('unknown protocol ' + options.protocol);
  }

  this._options = options;
  this._requestCount = 0;

  if (typeof this._options.requestTimeout === 'undefined') {
    this._options.requestTimeout = ms('2s');
  }

  this._pendingRequests = 0;

  this._sendRequestSafe = disyuntor(this._sendRequest.bind(this), _.extend({
    name: 'baas.client',
    timeout: options.requestTimeout,
    onTrip: (err) => {
      this.emit('breaker_error', err);
    }
  }, options.breaker || {} ));

  this.connect(done);
}

util.inherits(BaaSClient, EventEmitter);

BaaSClient.prototype.connect = function (done) {
  const options = this._options;
  const client = this;

  client.emit('connect');
  client.emit('ready');

  this.socket = this._socketLib

  client.once('ready', done || _.noop);
};

BaaSClient.prototype.hash = function (password, salt, callback) {
  //Salt is keep for api-level compatibility with node-bcrypt
  //but is enforced in the backend.
  if (typeof salt === 'function') {
    callback = salt;
  }

  if (!password) {
    return setImmediate(callback, new Error('password is required'));
  }

  const request = {
    'password':  password,
    'operation': RequestMessage.Operation.HASH,
  };

  this._sendRequestSafe(request, (err, response) => {
    callback(err, response && response.hash);
  });
};

BaaSClient.prototype.compare = function (password, hash, callback) {
  if (!password) {
    return setImmediate(callback, new Error('password is required'));
  }

  if (!hash) {
    return setImmediate(callback, new Error('hash is required'));
  }

  var request = {
    'password':  password,
    'hash':      hash,
    'operation': RequestMessage.Operation.COMPARE,
  };

  this._sendRequestSafe(request, (err, response) => {
    callback(err, response && response.success);
  });
};

BaaSClient.prototype._sendRequest = function (params, callback) {
  if (!callback) {
    return setImmediate(callback, new Error('callback is required'));
  }

  var pb_request;
  try {
    pb_request = RequestMessage.create(_.extend({
      'id': randomstring.generate(7)
    }, params));
    let err = RequestMessage.verify(pb_request);
    if (err) {
      throw new Error(err);
    }
  } catch (err) {
    return callback(err);
  }

  this._requestCount++;
  this._pendingRequests++;

  this._socketLib.post(
    this._options.url,
    RequestMessage.encodeDelimited(pb_request).finish(),
    {
      responseType: 'arraybuffer',
      headers: {'Content-Type': 'application/octet-stream'}
    }).then(function (response) {
      this._pendingRequests--;
      if (this._pendingRequests === 0) {
        this.emit('drain');
      }

      if (response.busy) {
        return callback(new Error('baas server is busy'));
      }

      callback(null, ResponseMessage.decodeDelimited(response.data));

    }).catch(function (response) {
      console.log(response);
    })
};

BaaSClient.prototype.disconnect = function () {
};

module.exports = BaaSClient;
